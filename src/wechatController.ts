/**
 * example controller of wechat server
 * Created by enixjin on 4/12/17.
 */

import {Controller, Get, logger, Post, serviceException} from "@jinyexin/core";
import {abstractHandler} from "./handler/abstractHandler";
import {defaultHandler} from "./handler/defaultHandler";
import {wechatService} from "./service/wechatService";
import {wechatUtils} from "./util/wechatUtils";


@Controller("")
export class WechatController {

    public handler: abstractHandler = new defaultHandler();

    @Post({
        url: "/transfer",
        auth: false,
        callbacks: [wechatUtils.checkLocalhost]
    })
    async transfer(req, res) {
        let message = JSON.parse(req.body);
        return wechatService.transfer(message.openid, message.amount, message.trade_no)
            .then(
                data => data.xml,
            );
    }

    @Post({
        url: "/pay",
        auth: false,
    })
    async pay(req, res, next) {
        return this.handler.handlePayRequest(req, res);
    }

    @Post({
        url: "/unifiedorder",
        auth: false,
    })
    async unifiedOrder(req, res, next) {
        let request = JSON.parse(req.body);
        request.spbill_create_ip = req.headers['x-real-ip'] || req.connection.remoteAddress;
        logger.warn(`get real ip for unifiedorder, result is:${request.spbill_create_ip}`);
        return wechatService.createUnifiedOrder(request)
            .then(
                data => data.xml
            );
    }

    @Get({
        url: "/",
        auth: false,
    })
    async checkSignature(req, res, next) {
        return this.handler.handleSignature(req, res);

    }

    @Post({
        url: "/",
        auth: false,
    })
    async getMessage(req, res, next) {
        return this.handler.handleRequest(req, res);
    }

    @Post({
        url: "/templateMessage",
        auth: false,
    })
    async sendTemplateMessage(req, res, next) {
        return wechatService.sendTemplateMessage(JSON.parse(req.body));
    }

    @Get({
        url: "/signature",
        auth: false,
    })
    private signature(req, res, next) {
        return wechatService.getSignature(req.query.url)
            .then(signatureInfo => {
                signatureInfo.appID = global.config.wechat.appID;
                res.json(signatureInfo);
            })
    }

    @Post({
        url: "/paySign",
        auth: false,
    })
    async paySign(req, res, next) {
        return {paySign: wechatService.getPaySign(JSON.parse(req.body))};
    }

    @Get({
        url: "/auth_userinfo",
        auth: false,
    })
    private auth_userinfo(req, res, next) {
        let code = req.query.code;
        let state = req.query.state;
        if (code && state) {
            return wechatService.getWebAccessToken(code)
                .then((result) => wechatService.getUserByWebToken(result.access_token, result.openid))
                .then((resp) => {
                    let user = JSON.parse(resp);
                    logger.debug(`user:${JSON.stringify(user)}`);
                    res.jsonp(user);
                })
                .catch((err) => {
                    res.end(err);
                })
        } else {
            throw new serviceException("missing code or state!", 400);
        }
    }

    @Get({
        url: "/auth",
        auth: false,
    })
    private async auth(req, res, next) {
        let code = req.query.code;
        let state = req.query.state;
        if (code && state) {
            let result = await wechatService.getWebAccessToken(code)
            return {openid: result.openid};
        } else {
            throw new serviceException("missing code or state!", 400);
        }
    }

    @Get({
        url: "/authAndRegister",
        auth: false,
    })
    private authAndRegister(req, res, next) {
        let code = req.query.code;
        let state = req.query.state;
        if (code && state) {
            return wechatService.getWebAccessToken(code)
                .then((result) => wechatService.getUserByWebToken(result.access_token, result.openid))
                .then((resp) => {
                    let user = JSON.parse(resp);
                    let config: any = global.config;
                    let options = {
                        hostname: config.domainServer,
                        port: config.domainPort,
                        path: config.domainWechatLoginUrl,
                        method: 'POST',
                        headers: {
                            'Content-Type': 'application/json'
                        }
                    };
                    logger.debug(`user:${JSON.stringify(user)}`);
                    wechatUtils.sendHTTPRequest(
                        options,
                        JSON.stringify({
                            open_id: user.openid,
                            nickname: user.nickname,
                            sex: user.sex,
                            city: user.city,
                            province: user.province,
                            country: user.country,
                            headimgurl: user.headimgurl
                        }),
                        data => {
                            logger.debug(`get login data from domain server:${data}, redirect to web:${config.webServer}/${config.webloginurl}/${JSON.parse(data).token};id=${JSON.parse(data).id};state=${state}`);
                            res.redirect(`http://${config.webServer}/${config.webloginurl}/${JSON.parse(data).token};id=${JSON.parse(data).id};state=${state}`);
                        },
                        err => {
                            res.end(JSON.stringify(err));
                        });
                })
                .catch((err) => {
                    res.end(err);
                })
        } else {
            throw new serviceException("missing code or state!", 400);
        }
    }
}
