/**
 * Created by enixjin on 4/12/17.
 */
export * from "./xml/eventMessage";
export * from "./xml/payMessage";
export * from "./xml/payResponseMessage";
export * from "./xml/textMessage";
export * from "./xml/transferMessage";
export * from "./xml/unifiedorder";
export * from "./xml/wechatXMLMessage";

export * from "./json/templateMessage";
export * from "./json/wechatJSONMessage";
export * from "./json/menuCreateMessage";