/**
 * Created by enixjin on 4/20/17.
 */

import {wechatXMLMessage} from "./wechatXMLMessage";

export class unifiedorder extends wechatXMLMessage {
    appid: string;
    attach?: string;
    body: string;
    mch_id: string;
    device_info: string;
    detail?: string;
    nonce_str: string;
    notify_url: string;
    openid: string;
    out_trade_no: string;
    spbill_create_ip: string;
    total_fee: number;
    trade_type: string = "JSAPI";
    sign: string;
}