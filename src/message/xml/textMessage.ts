/**
 * Created by enixjin on 4/12/17.
 */

import {wechatXMLMessage} from "./wechatXMLMessage";

export class textMessage extends wechatXMLMessage {
    ToUserName: string;
    FromUserName: string;
    CreateTime: number;
    MsgType = "text";
    Content: string;
}