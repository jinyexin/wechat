/**
 * Created by enixjin on 4/21/17.
 */
import {wechatXMLMessage} from "./wechatXMLMessage";

export class payResponseMessage extends wechatXMLMessage {
    return_code: string;
    return_msg: string;
}