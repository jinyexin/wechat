/**
 * Created by enixjin on 4/12/17.
 */

import {wechatXMLMessage} from "./wechatXMLMessage";

export class eventMessage extends wechatXMLMessage {
    ToUserName: string;
    FromUserName: string;
    CreateTime: number;
    MsgType: string;
    Event: string;
}