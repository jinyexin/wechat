/**
 * Created by enixjin on 11/9/17.
 */
import {wechatXMLMessage} from "./wechatXMLMessage";

export class transferMessage extends wechatXMLMessage {
    mch_appid: string;
    mchid: string;
    device_info?: string;
    nonce_str: string;
    sign: string;
    partner_trade_no: string;
    openid: string;
    check_name: string = "NO_CHECK";
    amount: number;
    desc: string;
    spbill_create_ip: string;
}