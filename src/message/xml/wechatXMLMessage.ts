/**
 * Created by enixjin on 4/12/17.
 */
import * as parser from "xml2json";

export class wechatXMLMessage {
    ToUserName?: string;
    FromUserName?: string;
    CreateTime?: number;
    MsgType?: string;

    toXMLString(): string {
        let data: any = {};
        for (let key in this) {
            if (typeof this[key] !== 'function') {
                data[key] = {"$t": this[key]};
            }
        }
        let xml = {xml: data};
        return parser.toXml(xml);
    }
}