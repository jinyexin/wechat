/**
 * Created by enixjin on 4/21/17.
 */

import {wechatXMLMessage} from "./wechatXMLMessage";

export class payMessage extends wechatXMLMessage {
    appid: string;
    attach: string;
    bank_type: string;
    fee_type: string;
    is_sbuscribe: string;
    mch_id: string;
    nonce_str: string;
    openid: string;
    out_trade_no: string;
    result_code: string;
    return_code: string;
    sign: string;
    sub_mch_id: string;
    time_end: string;
    total_fee: number;
    trade_type: string;
    transaction_id: string;
}