/**
 * Created by enixjin on 4/13/17.
 */
import {wechatJSONMessage} from "./wechatJSONMessage";

export class menuCreateMessage extends wechatJSONMessage {
    button: wechatMenuButton[];
}

export class wechatMenuButton {
    name: string;
    type?: "click"
        | "view"
        | "scancode_waitmsg"
        | "scancode_push"
        | "pic_sysphoto"
        | "pic_photo_or_album"
        | "pic_weixin"
        | "location_select"
        | "media_id"
        | "view_limited";
    key?: string;
    url?: string;

    sub_button?: wechatMenuButton[];
}