/**
 * Created by enixjin on 4/13/17.
 */

import {wechatJSONMessage} from "./wechatJSONMessage";

export class templateMessage extends wechatJSONMessage {
    touser: string;
    template_id: string;
    url: string;
    data: any;
    redirect_uri?: string;
    state?: string;
}