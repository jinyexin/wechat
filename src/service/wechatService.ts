import {menuCreateMessage, templateMessage, transferMessage, unifiedorder} from "../message";
import {wechatUtils} from "..";
import * as parser from "xml2json";
import {RequestOptions} from "https";
import {logger} from "@jinyexin/core";

let fs = require("fs");


/**
 * Created by enixjin on 4/13/17.
 */

export class wechatService {

    static sendTemplateMessage(message: templateMessage): Promise<any> {
        if (!message.url) {
            message.url = `https://open.weixin.qq.com/connect/oauth2/authorize?appid=${global.config.wechat.appID}` +
                "&redirect_uri=" + message.redirect_uri +
                "&response_type=code&scope=snsapi_userinfo&state=" + message.state + "#wechat_redirect";
        }
        delete message.redirect_uri;
        delete message.state;
        return new Promise((resolve, reject) => {
            let options = {
                method: 'POST',
                path: `/cgi-bin/message/template/send`,
                headers: {'Content-Type': 'application/json'}
            };
            wechatUtils.sendWechatRequest(options, JSON.stringify(message), data => {
                resolve(data);
            }, err => {
                reject(err);
            });
        });
    }

    static pushMenu(message: menuCreateMessage): Promise<any> {
        return new Promise((resolve, reject) => {
            let options = {
                method: "POST",
                path: `/cgi-bin/menu/create`,
                headers: {'Content-Type': 'application/json'}
            };
            wechatUtils.sendWechatRequest(options, JSON.stringify(message), data => {
                resolve(data);
            }, err => {
                reject(err);
            }, true);
        });
    }

    static transfer(openid: string, amount: number, trade_no: string): Promise<any> {
        let message = new transferMessage();
        message.mch_appid = global.config.wechat.appID;
        message.mchid = global.config.wechat.mch_id;
        message.device_info = "WEB";
        message.nonce_str = wechatUtils.createNonceStr();
        message.partner_trade_no = trade_no;
        message.openid = openid;
        message.check_name = "NO_CHECK";
        message.amount = amount;
        message.desc = "提现";
        message.spbill_create_ip = "14.23.150.211";

        message.sign = wechatUtils.signObject(message);

        return new Promise((resolve, reject) => {
            let options: RequestOptions = {
                method: "POST",
                hostname: "api.mch.weixin.qq.com",
                path: `/mmpaymkttransfers/promotion/transfers`,
                headers: {'Content-Type': 'application/xml'},
                key: fs.readFileSync(global.config.wechat.ssl_key),
                cert: fs.readFileSync(global.config.wechat.ssl_cert),
                // ca: fs.readFileSync(global.config.wechat.ca),
                agent: false
            };
            wechatUtils.sendWechatRequest(options, message.toXMLString(), data => {
                logger.debug(`transfer result ${JSON.stringify(data)}`);
                resolve(JSON.parse(parser.toJson(data)));
            }, err => {
                reject(err);
            }, false);
        });
    }

    static getWebAccessToken(code: string): Promise<any> {
        return wechatUtils.getWebAccessToken(code);
    }

    static getSignature(url: string): Promise<any> {
        return wechatUtils.getJSAPITicket().then(ticket => wechatUtils.sign(ticket, url));
    }

    static getPaySign(message: any) {
        return wechatUtils.signObject(message);
    }

    static getUserByWebToken(accessToken: string, openID: string): Promise<any> {
        return new Promise((resolve, reject) => {
            let options = {
                path: `/sns/userinfo?access_token=${accessToken}&openid=${openID}&lang=zh_CN`,
                headers: {'Content-Type': 'application/json'}
            };
            wechatUtils.sendWechatRequest(options, null, data => {
                resolve(data);
            }, err => {
                reject(err);
            }, false);
        });
    }

    static createUnifiedOrder(_message: unifiedorder): Promise<any> {
        let message = new unifiedorder();
        message.attach = _message.attach;
        message.body = _message.body;
        message.detail = _message.detail;
        message.openid = _message.openid;
        message.out_trade_no = _message.out_trade_no;
        message.spbill_create_ip = _message.spbill_create_ip;
        message.total_fee = _message.total_fee;
        message.device_info = "WEB";

        message.appid = global.config.wechat.appID;//wx2421b1c4370ec43b

        message.mch_id = global.config.wechat.mch_id;
        message.nonce_str = wechatUtils.createNonceStr();
        message.notify_url = global.config.wechat.notify_url;
        message.trade_type = "JSAPI";

        message.sign = wechatUtils.signObject(message);
        return new Promise((resolve, reject) => {
            let options = {
                method: "POST",
                hostname: "api.mch.weixin.qq.com",
                path: `/pay/unifiedorder`
            };
            wechatUtils.sendWechatRequest(options, message.toXMLString(), data => {
                logger.debug(`unifiedorder result ${JSON.stringify(data)}`);
                resolve(JSON.parse(parser.toJson(data)));
            }, err => {
                reject(err);
            }, false);
        });
    }


}
