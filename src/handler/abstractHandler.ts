/**
 * Created by enixjin on 4/13/17.
 */

import * as parser from "xml2json";
import {eventMessage, payMessage, payResponseMessage, textMessage, wechatXMLMessage} from "../message";
import {logger} from "@jinyexin/core";
import {wechatUtils} from "../util/wechatUtils";

export abstract class abstractHandler {

    handleSignature(req, res) {
        let signature = req.query.signature;
        let timestamp = req.query.timestamp;
        let nonce = req.query.nonce;
        let echostr = req.query.echostr;

        if (wechatUtils.checkWechatSignature(signature, timestamp, nonce, global.config.wechat.token)) {
            logger.debug("check wechat signature success!");
            res.end(echostr);
        } else {
            logger.error("check wechat signature fail!");
            res.end('invalid request');
        }
    }

    handleRequest(req, res) {
        try {
            let message = JSON.parse(parser.toJson(req.body)).xml as wechatXMLMessage;
            logger.debug(`acquire message from wechat:${JSON.stringify(message)}`);
            res.writeHead(200, {'Content-Type': 'application/xml'});
            let response = new textMessage();
            response.ToUserName = message.FromUserName;
            response.FromUserName = message.ToUserName;
            response.CreateTime = Math.floor(new Date().getTime() / 1000);
            // TODO default response should be empty
            // response.Content = ``;
            response.Content = `DEBUG:尚未处理的消息类型:MsgType[${message.MsgType}]`;

            if (message.MsgType === "text") {
                response = this.handleTextMessage(message as textMessage, response);
            } else if (message.MsgType === "event") {
                let em = message as eventMessage;
                response.Content += `,Event[${em.Event}]`;
                if (em.Event === "subscribe") {
                    response = this.handleEventSubscribeMessage(em, response);
                }
            }
            logger.debug(`send response:${JSON.stringify(response)}`);
            res.end(response.toXMLString());
        } catch (e) {
            logger.error(e);
            res.end("");
        }
    }

    handlePayRequest(req, res) {
        let message = JSON.parse(parser.toJson(req.body)).xml as payMessage;
        logger.debug(`acquire pay from wechat:${JSON.stringify(message)}`);
        res.writeHead(200, {'Content-Type': 'application/xml'});
        this.handelPayMessage(message).then(
            response => {
                logger.debug(`send pay response:${JSON.stringify(response)}`);
                res.end(response.toXMLString());
            },
            e => {
                // should not go here since handlePayMessage will always resolve
                logger.error(e);
                let errorResponse = new payResponseMessage();
                errorResponse.return_code = "FAIL";
                errorResponse.return_msg = e;
                logger.error(`send pay failed response:${JSON.stringify(errorResponse)}`);
                res.end(errorResponse.toXMLString());
            }
        );


    }

    protected abstract handleTextMessage(message: textMessage, response: textMessage): textMessage;

    protected abstract handleEventSubscribeMessage(message: eventMessage, response: textMessage): textMessage;

    protected abstract handelPayMessage(message: payMessage): Promise<payResponseMessage>;
}