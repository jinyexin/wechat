/**
 * Created by enixjin on 4/13/17.
 */

import {eventMessage} from "../message/xml/eventMessage";
import {payMessage} from "../message/xml/payMessage";
import {payResponseMessage} from "../message/xml/payResponseMessage";
import {textMessage} from "../message/xml/textMessage";
import {abstractHandler} from "./abstractHandler";
import {wechatUtils} from "../util/wechatUtils";
import {logger} from "@jinyexin/core";


export class defaultHandler extends abstractHandler {

    handleTextMessage(message: textMessage, response: textMessage): textMessage {
        response.Content = `欢迎互动`;
        return response;
    }


    handleEventSubscribeMessage(message: eventMessage, response: textMessage): textMessage {
        response.Content = `欢迎加入`;
        return response;
    }

    handelPayMessage(message: payMessage): Promise<payResponseMessage> {
        let config: any = global.config;
        let response = new payResponseMessage();

        let options = {
            hostname: config.domainServer,
            port: config.domainPort,
            path: config.domainOrderAPI + "/" + message.out_trade_no,
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json'
            }
        };
        return new Promise((resolve, reject) => {
            wechatUtils.sendHTTPRequest(
                options,
                null,
                data => {
                    logger.debug(`get data from domain server:${data}`);
                    response.return_code = "SUCCESS";
                    resolve(response);
                },
                err => {
                    response.return_code = "FAIL";
                    response.return_msg = JSON.stringify(err);
                    logger.error(JSON.stringify(err));
                    // will always resolve result to wechat
                    resolve(response);
                });
        });
    }
}