/**
 * Created by enixjin on 4/12/17.
 */
import {RequestOptions} from "https";
import {logger} from "@jinyexin/core";

export namespace wechatUtils {
    let crypto = require('crypto');
    let https = require("https");
    let http = require("http");

    let accessToken = "";
    let tokenLastUpdate: Date = new Date();

    let jsapi_ticket = "";
    let ticketLastUpdate: Date = new Date();

    export function checkWechatSignature(signature, timestamp, nonce, token) {
        let tmpArr = [token, timestamp, nonce];
        tmpArr.sort();
        let tmpStr = tmpArr.join('');
        let shasum = crypto.createHash('sha1');
        shasum.update(tmpStr);
        let shaResult = shasum.digest('hex');
        return shaResult === signature;
    }

    export function sign(jsapi_ticket, url) {
        let nonceStr = createNonceStr();
        let timestamp = createTimestamp();
        let result: any = {
            jsapi_ticket,
            nonceStr,
            timestamp,
            url
        };
        let tmpStr = raw(result);
        let shasum = crypto.createHash('sha1');
        shasum.update(tmpStr);
        let signature = shasum.digest('hex');
        return {
            nonceStr,
            timestamp,
            signature
        };
    }

    export function signObject(message: any) {
        let tmpStr = raw(message, true, false) + "&key=" + global.config.wechat.key;
        logger.debug(`sign string:${tmpStr}`);
        return crypto.createHash('md5').update(tmpStr).digest('hex').toUpperCase();
    }

    export function getAccessToken(): Promise<string> {
        let cur = new Date();
        return new Promise((resolve, reject) => {
            // renew
            if (accessToken === "" || (cur.getTime() - tokenLastUpdate.getTime()) >= global.config.wechat.tokenTimeout) {
                logger.debug("renew wechat access token...");
                let options = {
                    method: 'GET',
                    path: `/cgi-bin/token?grant_type=client_credential&appid=${global.config.wechat.appID}&secret=${global.config.wechat.appsecret}`
                };

                sendWechatRequest(options, null, (output) => {
                    try {
                        let body = JSON.parse(output);
                        if (body.access_token) {
                            accessToken = body.access_token;
                            tokenLastUpdate = new Date();
                            logger.debug(`new wechat access token:${accessToken}`);
                            resolve(accessToken);
                        }
                    } catch (e) {
                        logger.error(`wechat token parse error${e}`);
                        reject(e);
                    }
                }, (e) => {
                    logger.error(`fail to get new token, err:${e}`);
                    reject(e);
                }, false);

            } else {
                logger.debug("reuse wechat access token...");
                resolve(accessToken);
            }
        });
    }

    export function getJSAPITicket(): Promise<string> {
        let cur = new Date();
        return new Promise((resolve, reject) => {
            if (jsapi_ticket === "" || (cur.getTime() - ticketLastUpdate.getTime()) >= global.config.wechat.tokenTimeout) {
                logger.debug("renew wechatjsapi ticket token...");
                getAccessToken().then(
                    (token) => {
                        let options = {
                            hostname: "api.weixin.qq.com",
                            port: 443,
                            method: "GET",
                            path: `/cgi-bin/ticket/getticket?access_token=${token}&type=jsapi`
                        };
                        sendWechatRequest(options, null, (output) => {
                            try {
                                let body = JSON.parse(output);
                                if (body.ticket) {
                                    jsapi_ticket = body.ticket;
                                    ticketLastUpdate = new Date();
                                    logger.debug(`new jsapi ticket:${jsapi_ticket}`);
                                    resolve(jsapi_ticket);
                                } else {
                                    logger.error(`wechat jsapi ticket format error:${body}`);
                                    reject("format error");
                                }
                            } catch (e) {
                                logger.error(`wechat jsapi ticket parse error${e}`);
                                reject(e);
                            }
                        }, (e) => {
                            logger.error(`fail to get new jsapi ticket, err:${e}`);
                            reject(e);
                        }, false);
                    }
                );
            } else {
                logger.debug("reuse jsapi ticker token...");
                resolve(jsapi_ticket);
            }
        });
    }

    export function getWebAccessToken(code: string): Promise<any> {
        return new Promise((resolve, reject) => {
            // renew
            logger.debug("get wechat web access token...");
            let options = {
                method: 'GET',
                path: `/sns/oauth2/access_token?appid=${global.config.wechat.appID}&secret=${global.config.wechat.appsecret}&code=${code}&grant_type=authorization_code`
            };

            sendWechatRequest(options, null, (output) => {
                try {
                    logger.debug(`wechat web access token result:${output}`);
                    let body = JSON.parse(output);
                    if (body.access_token) {
                        resolve(body);
                    } else {
                        logger.error(`wechat web token error${body}`);
                        reject("fail to get access token from response!");
                    }
                } catch (e) {
                    logger.error(`wechat web token parse error${e}`);
                    reject(e);
                }
            }, (e) => {
                logger.error(`fail to get new web token, err:${e}`);
                reject(e);
            }, false);
        });
    }

    export function sendWechatRequest(options: RequestOptions, data: any, successCallback: (data) => void, failCallback: (err) => void, appendToken: boolean = true) {
        let pre = appendToken ? getAccessToken : () => Promise.resolve("");
        pre().then(
            (token) => {
                if (!options.method) {
                    options.method = "GET";
                }
                if (!options.hostname) {
                    options.hostname = "api.weixin.qq.com";
                }
                options.port = 443;
                if (appendToken) {
                    options.path += "?access_token=" + token;
                }
                logger.debug(`sending wechat request with options:${JSON.stringify(options)}`);
                if (data) {
                    logger.debug(`sending data to wechat:${data}`);
                }
                let req = https.request(options, (res) => {
                    let output = "";
                    res.on('data', (d) => {
                        output += d;
                    });
                    res.on("end", () => {
                        successCallback(output);
                    });

                }).on('error', (e) => {
                    failCallback(e);
                });
                if (data) {
                    req.end(data);
                } else {
                    req.end();
                }
            }
        );
    }


    export function sendHTTPRequest(options: RequestOptions, data: any, successCallback: (data) => void, failCallback: (err) => void, appendToken: boolean = true) {
        logger.debug(`sending http request with options:${JSON.stringify(options)}`);
        if (data) {
            logger.debug(`sending data:${data}`);
        }
        let req = http.request(options, (res) => {
            let output = "";
            res.on('data', (d) => {
                output += d;
            });
            res.on("end", () => {
                successCallback(output);
            });

        }).on('error', (e) => {
            failCallback(e);
        });
        if (data) {
            req.end(data);
        } else {
            req.end();
        }

    }

    export function createNonceStr() {
        return Math.random().toString(36).substr(2, 15);
    }

    export function createTimestamp() {
        return Math.floor(new Date().getTime() / 1000);
    }

    export function raw(args, sort: boolean = true, toLowerCase: boolean = true) {
        let keys = Object.keys(args);
        if (sort) {
            keys = keys.sort();
        }
        let newArgs = {};
        keys.forEach(function (key) {
            const _key = toLowerCase ? key.toLowerCase() : key;
            newArgs[_key] = args[key];
        });

        let string = '';
        for (let k in newArgs) {
            string += '&' + k + '=' + newArgs[k];
        }
        string = string.substr(1);
        return string;
    }

    export function checkLocalhost(req, res, next) {
        /*
        * with nginx config like following:
        * location / {
            proxy_pass http://localhost:9801;
            proxy_http_version 1.1;
            proxy_set_header Upgrade $http_upgrade;
            proxy_set_header Connection 'upgrade';
            proxy_set_header Host $host;
            proxy_set_header X-Real-IP $remote_addr;
            proxy_cache_bypass $http_upgrade;
        }
        * */
        let ip = req.headers['x-real-ip'] || req.ip;
        logger.debug(`getting transfer request from ip:${ip}`);
        if (ip === "::1" || ip === "127.0.0.1" || ip === "::ffff:127.0.0.1") {
            logger.debug(`pass localhost check`);
            next();
        } else {
            logger.error(`${ip} is trying to access that only open to localhost!!!`);
            res.status(500).end("access deny");
        }
    }

}