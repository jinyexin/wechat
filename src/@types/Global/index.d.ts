/**
 * Created by enixjin on 1/12/17.
 */
declare module NodeJS {
    interface Global {
        config: any;
        dependencyInjectionContainer: Map<string, any>;
    }
}