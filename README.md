# wechat

A simple wechat server lib
 
[source code](https://gitlab.com/jinyexin/wechat)    

## Prerequisites:

   * npm i -g typescript 
   
   * you need set "proxy_set_header X-Real-IP $remote_addr;" in your nginx config, so unifiedorder will have correct ip

## changelog

See [CHANGELOG](https://gitlab.com/jinyexin/wechat/blob/master/CHANGELOG.md) 

## about node-gyp

   see https://github.com/nodejs/node-gyp#installation, if you have trouble with wechat server xml2json support
   
## exposed api
    
    {POST} /transfer
    {POST} /pay
    {POST} /unifiedorder
    {POST} /templateMessage
    {POST} /paySign
    {GET}  /signature
    {GET}  /auth                //scope=snsapi_base can only get openid
    {GET}  /auth_userinfo       //scope=snsapi_userinfo can send second request to get user info
    {GET}  /authAndRegister
    
    
    {POST} /               //get message from wechat
    {GET}  /               //checkSignature from wechat

## example config

    //wechat config
    config.wechat = {};
    config.wechat.token = "enixjin";
    config.wechat.tokenTimeout = (90 * 60) * 1000;//refresh token every 90 mins
    config.wechat.appID = "123456789";
    config.wechat.appsecret = "123456789";
    config.wechat.mch_id = "10000100";
    config.wechat.key = "123456789";
    config.wechat.notify_url = "http://wechat.yoursite.com/pay";
    
    
    //domain config for register
    config.domainServer = "domain.yoursite.com";
    config.domainPort = 9802;
    config.domainWechatLoginUrl = "/loginWechat";
    config.domainOrderAPI = "/api/purchaseOrders/order";

## example code
    
    create you own handle or use default handle
    //default handler
    export class defaultHandler extends abstractHandler {
    
        handleTextMessage(message: textMessage, response: textMessage): textMessage {
            response.Content = `欢迎互动`;
            return response;
        }
    
    
        handleEventSubscribeMessage(message: eventMessage, response: textMessage): textMessage {
            response.Content = `欢迎加入`;
            return response;
        }
        
        ...
    }
